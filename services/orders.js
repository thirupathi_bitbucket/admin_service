var orders = require('../models/orders');
module.exports = {
    create: function(req, res, callback) {
        var order = new orders(req.body);
        order.total = req.body.total;
        order.tax = req.body.tax;
        order.status = req.body.status;
        order.products = req.body.products;
        order.created_at=new Date()
        order.save(function(err, data) {
          var customError ="";
          if (err) {
                console.log('Error Inserting New Data');
                if (err.name == 'ValidationError') {
                    for (field in err.errors) {
                        customError +=err.errors[field].message+';'; 
                    }
                } else if(err.name == 'MongoError' && err.code ==11000){
                    customError="itemCode and itemName combination is already exist";
                }
            }
            callback(customError, data);
        });

    },
    findAll: function(req, res, callback) {
        orders.find({}, function(err, docs) {
            callback(err, docs);
        });        
    },
    findById: function(req, res, callback) {
        orders.find({_id:req.params.id}, function(err, docs) {
            callback(err, docs);
        });        
    },
    search: function(req, res, callback) {
        var searchstring = req.params.searchstring;
        console.log(searchstring);
        orders.find(
            {$or: [
                    {status: {$regex: searchstring, $options: 'i'}},
                    {total: {$regex: searchstring, $options: 'i'}},
                    {customerEmail: {$regex: searchstring, $options: 'i'}},
                    {customerPhone: {$regex: searchstring, $options: 'i'}}
                ]
        })
        .exec(function(err, docs) {
            callback(err, docs);
        });        
    },
    update: function(req, res, callback) {
        var active = req.body.active==undefined ? true : req.body.active;
        var expiryDate = req.body.expiryDate ==''? null : new Date(req.body.expiryDate);
        var whereCondition = {_id: parseInt(req.params.id)};
        var product = {
            itemName:req.body.itemName,
            itemCode:req.body.itemCode,
            aliasName:req.body.aliasName,
            itemNoSize:req.body.itemSize,
            groupCode:req.body.groupCode,
            manufacturer:req.body.manufacturer,
            itemLocation:req.body.itemLocation,
            itemDiscount:req.body.itemDiscount,
            customerPoints:req.body.itemPoints,
            active:active,
            stockAndValue: {
                UOM:req.body.measureUnit,
                openingStock:req.body.openStock,
                stockValue:req.body.stockValue,
                reOrderQty:req.body.reorderQuantity,
                damageOpStock:req.body.damagedStock,
                expiryDate: expiryDate
            },
            priceDetails: {
                purchasePrice: req.body.purchasePrice,
                purchasePricePer:req.body.purchasePricePer,
                profit:req.body.profitPercentage,
                salesPrice: req.body.salesPrice,
                salesPricePer:req.body.salesPricePer,
                MRPPrice:req.body.mrpPrice,
                MRPPricePer:req.body.MRPPricePer
            },  
            dutyDetails: {
                SGST: req.body.sGst,
                CGST:req.body.cGst,
                IGST:req.body.iGst,
                HSN_details:req.body.hsnDescription,
                HSN_code:req.body.hsnCode
            }
        }
        orders.findByIdAndUpdate(req.params.id, {$set: product}, function(err, data) {
          var customError ="";
          if (err) {
                if (err.name == 'ValidationError') {
                    for (field in err.errors) {
                        customError +=err.errors[field].message+';'; 
                    }
                } else if(err.name == 'MongoError' && err.code ==11000){
                    customError="itemCode and itemName combination is already exist";
                } else {
                    customError=err;
                }
            }
            callback(customError, data);
        });

    }     
}
