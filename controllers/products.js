var productService = require('../services/products');
module.exports.controller = function(app) {
    app.post('/v1/product', function(req, res) {
        productService.createProduct(req, res, function(err, data){
            res.status(200).json({ error: err, data: data });
        });
    });
    app.get('/v1/products', function(req, res) {
        productService.findAllProducts(req, res, function(err, data){
            res.status(200).json({ error: err, data: data });
        });
    });
    app.get('/v1/product/:id', function(req, res) {
        productService.findByProductId(req, res, function(err, data){
            res.status(200).json({ error: err, data: data });
        });
    });
    app.get('/v1/product/search/:searchstring', function(req, res) {
        productService.search(req, res, function(err, data){
            res.status(200).json({ error: err, data: data });
        });
    });
    app.put('/v1/product/:id', function(req, res) {
        productService.updateProduct(req, res, function(err, data){
            res.status(200).json({ error: err, data: data });
        });
    });    
}
