var orderService = require('../services/orders');
module.exports.controller = function(app) {
    app.post('/v1/order', function(req, res) {
        orderService.create(req, res, function(err, data){
            res.status(200).json({ error: err, data: data });
        });
    });
    app.get('/v1/orders', function(req, res) {
        orderService.findAll(req, res, function(err, data){
            res.status(200).json({ error: err, data: data });
        });
    });
    app.get('/v1/order/:id', function(req, res) {
        orderService.findById(req, res, function(err, data){
            res.status(200).json({ error: err, data: data });
        });
    });
    app.get('/v1/order/search/:searchstring', function(req, res) {
        orderService.search(req, res, function(err, data){
            res.status(200).json({ error: err, data: data });
        });
    });
    app.put('/v1/order/:id', function(req, res) {
        orderService.update(req, res, function(err, data){
            res.status(200).json({ error: err, data: data });
        });
    });    
}
